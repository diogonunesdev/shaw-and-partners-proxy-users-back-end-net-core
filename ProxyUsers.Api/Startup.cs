using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProxyUsers.Api.Application.IocExtensions;
using ProxyUsers.Api.Core.Cors;
using ProxyUsers.Infra.Ioc;
using System.IO.Compression;
using ProxyUsers.Api.Core.Filters;
using Microsoft.Extensions.Logging;

namespace ProxyUsers.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            #region [Compression]

            services.Configure<BrotliCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;

            });

            #endregion [Compression]

            #region [User IOC]

            services.ConfigureUserIocServices(Configuration);

            #endregion [USer IOC]

            #region [Application IOC]

            services.ConfigureApplicationIocServices();

            #endregion

            #region [CORS]

            services.ConfigureCors();

            #endregion [CORS]

            #region [Filters]

            services.ConfigureFilters();

            #endregion [Filters]

            #region [Dot Net

            #region [ResponseCache]

            services.AddResponseCaching();

            #endregion [ResponseCache]

            services.AddScoped<ILogger, Logger<string>>();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProxyUsers.Api", Version = "v1" });
            });

            #endregion [Dot Net]
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Shaw And Partners - ProxyUsers.Api v1"));


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(CorsConfiguration.FULL_ACCESS_POLICY);

            app.UseAuthorization();

            app.UseResponseCaching();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            #region [PaginationMiddleware]
            #endregion [PaginationMiddleware]
        }
    }
}
