﻿namespace ProxyUsers.Api.Base.CQ
{
    public interface IResponse
    {
    }

    public interface IPaginatedResponse :IResponse
    {
        string next_page_url { get; set; }
    }
}