﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProxyUsers.Api.Base.CQ;
using ProxyUsers.Api.Core.Cors;
using ProxyUsers.Api.Core.Filters;
using ProxyUsers.Domain.Contracts;
using System;
using System.Threading.Tasks;

namespace ProxyUsers.Api.Base.Controller
{
    [EnableCors(CorsConfiguration.FULL_ACCESS_POLICY)]
    [ServiceFilter(typeof(ErrorFilterAttribute))]
    public abstract class BaseController: ControllerBase
    {
        #region [ctor]

        private readonly IDomainNotification _domainNotification;

        #endregion [ctor]

        #region [prop]

        public BaseController(IDomainNotification domainNotification)
        {
            _domainNotification = domainNotification;

        }
        #endregion [prop]

        protected async Task<IActionResult> ExecuteAsync(Func<Task<IResponse>> executeFunction)
        {
            //HasNotification catch by the ErrorFilter, not neet to process the remaining code
            if (_domainNotification.HasNotification)
                return null;

            var response = await executeFunction();
            
            return response == null
             ? BadRequest() 
             : Ok(response);
        }

        protected async Task<IActionResult> ExecuteGetAsync(Func<Task<IResponse>> executeFunction)
        {
            //HasNotification catch by the ErrorFilter, not neet to process the remaining code
            if (_domainNotification.HasNotification)
                return null;

            var response = await executeFunction();

            return response == null || response == default(IResponse)
             ? NoContent()
             : Ok(response);
        }
    }
}