﻿using Microsoft.Extensions.DependencyInjection;

namespace ProxyUsers.Api.Core.Filters
{
    public static class FilterExtensions
    {
        public static void ConfigureFilters(this IServiceCollection services)
        {
            services.AddScoped<ErrorFilterAttribute>();
            services.AddScoped<PaginationFilterAttribute>();
        }
    }
}
