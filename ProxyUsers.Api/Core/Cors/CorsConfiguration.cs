﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace ProxyUsers.Api.Core.Cors
{
    public static class CorsConfiguration 
    {
        public const string FULL_ACCESS_POLICY = "FULL_ACCESS_POLICY";
    }

    public static class CorsConfiruationExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(CorsConfiguration.FULL_ACCESS_POLICY,
                builder =>
                {
                    builder
                     .AllowAnyOrigin()
                     .AllowAnyHeader()
                     .AllowAnyMethod()
                     .SetPreflightMaxAge(TimeSpan.FromDays(5))
                     .Build();
                });
            });

        }
    }
}