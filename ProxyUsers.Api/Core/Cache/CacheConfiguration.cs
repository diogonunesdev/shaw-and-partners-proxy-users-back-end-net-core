﻿namespace ProxyUsers.Api.Core.Cache
{
    public static class CacheConfiguration
    {
        public const int CACHE_TWO_MINUTES_IN_SECONDS = 120 ;
    }
}