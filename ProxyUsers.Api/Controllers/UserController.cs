﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProxyUsers.Api.Application.Queries;
using ProxyUsers.Api.Base.Controller;
using ProxyUsers.Api.Core.Cache;
using ProxyUsers.Api.Core.Filters;
using ProxyUsers.Domain.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace MeuUserDigital.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public sealed class UserController : BaseController
    {
        #region  [prop]

        private readonly IGetUsersBySinceQuery _getUsersQuery;
        private readonly IGetDetailsByUserNameQuery _getDetailsByUserNameQuery;

        #endregion  [ctor]

        #region  [ctor]

        public UserController(IGetUsersBySinceQuery getUsersQuery,
            IGetDetailsByUserNameQuery getDetailsByUserNameQuery,
            IDomainNotification domainNotification) :base(domainNotification)
        {
            _getUsersQuery = getUsersQuery;
            _getDetailsByUserNameQuery = getDetailsByUserNameQuery;
        }

        #endregion  [ctor]

        /// <summary>
        /// Return all users
        /// </summary>
        /// <param name="since">since</param>
        /// <param name="pageSize">page size</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Return the users with since pagination, for empty return values you get 203 response</returns>
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ServiceFilter(typeof(PaginationFilterAttribute))]
        [ResponseCache(Duration = CacheConfiguration.CACHE_TWO_MINUTES_IN_SECONDS, NoStore = false, VaryByQueryKeys =new [] { "*" })]
        [Microsoft.AspNetCore.Mvc.HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] long since, [FromQuery] string pageSize, CancellationToken cancellationToken)
        {
            return await ExecuteGetAsync(async () => await _getUsersQuery.ExecuteGetAsync(cancellationToken));
        }

        /// <summary>
        /// Return the details of user by Login(username)
        /// </summary>
        /// <param name="username">login</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Return details of user, for empty return values you get 203 response</returns>
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ResponseCache(Duration = CacheConfiguration.CACHE_TWO_MINUTES_IN_SECONDS, NoStore = false, VaryByQueryKeys = new[] { "username" })]
        [Microsoft.AspNetCore.Mvc.HttpGet("{username}/details")]
        public async Task<IActionResult> GetUserDetails(string username, CancellationToken cancellationToken)
        {
            return await ExecuteGetAsync(async () => await _getDetailsByUserNameQuery.ExecuteGetAsync(new GetDetailsByUserNameRequest(username) ,cancellationToken));
        }
        
    }
}                                                  