﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProxyUsers.Api.Application.Queries;
using ProxyUsers.Api.Base.Controller;
using ProxyUsers.Api.Core.Cache;
using ProxyUsers.Domain.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace MeuUserDigital.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public sealed class UserRepositoryController : BaseController
    {
        #region  [prop]

        private readonly IGetRepositoriesByUserNameQuery _getRepositoriesByUserNameQuery;

        #endregion  [ctor]

        #region  [ctor]

        public UserRepositoryController( IDomainNotification domainNotification,
            IGetRepositoriesByUserNameQuery getRepositoriesByUserNameQuery) :base(domainNotification)
        {
            _getRepositoriesByUserNameQuery = getRepositoriesByUserNameQuery;
        }

        #endregion  [ctor]

        /// <summary>
        /// Return the repositories of user by Login(username)
        /// </summary>
        /// <param name="username">login</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Return repositories of user, for empty return values you get 203 response</returns>
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ResponseCache(Duration = CacheConfiguration.CACHE_TWO_MINUTES_IN_SECONDS, NoStore = false, VaryByQueryKeys = new[] { "username" })]

        [Microsoft.AspNetCore.Mvc.HttpGet("/api/user/{username}/repos")]
        public async Task<IActionResult> GetRepositories(string username, CancellationToken cancellationToken)
        {
            return await ExecuteGetAsync(async () => await _getRepositoriesByUserNameQuery.ExecuteGetAsync(new GetRepositoriesByUserNameRequest(username), cancellationToken));
        }
    }
}