﻿using Microsoft.Extensions.DependencyInjection;
using ProxyUsers.Api.Application.Queries;

namespace ProxyUsers.Api.Application.IocExtensions
{
    public static class UserIApplicationIocExtensions
    {
        public static void ConfigureApplicationIocServices(this IServiceCollection services)
        {
            services.AddScoped<IGetUsersBySinceQuery, GetUsersBySinceQuery>();
            services.AddScoped<IGetDetailsByUserNameQuery, GetDetailsByUserNameQuery>();
            services.AddScoped<IGetRepositoriesByUserNameQuery, GetRepositoriesByUserNameQuery>();
        }
    }
}
