﻿using ProxyUsers.Api.Base.CQRS;
using ProxyUsers.Domain.Contracts;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Core;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Infra.Data.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetDetailsByUserNameQuery : IGetDetailsByUserNameQuery
    {
        #region [prop]

        private readonly IUserRepository _userRepository;
        #endregion [prop]

        #region [ctor]

        public GetDetailsByUserNameQuery(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #endregion [ctor]


        public async Task<GetDetailsByUserNameResponse> ExecuteGetAsync(GetDetailsByUserNameRequest request, CancellationToken cancellationToken)
        {
            var response = await _userRepository.GetUserByNameAsync(request.UserName, cancellationToken);
            
            if (response == null || response.id ==0 || response ==  default(UserDetailedDto))
                return null;

            return new GetDetailsByUserNameResponse(response);
        }

     
    }
}