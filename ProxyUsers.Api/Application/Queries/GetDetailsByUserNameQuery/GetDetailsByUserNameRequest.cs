﻿using ProxyUsers.Api.Base.CQ;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetDetailsByUserNameRequest : IRequest
    {
        #region [prop]
       public string UserName { get; set; }

        #endregion [prop]

        #region [ctor]

        public GetDetailsByUserNameRequest(string userName)
        {
            UserName = userName;
        }

        #endregion [ctor]
    }
}