﻿using ProxyUsers.Api.Base.CQ;
using ProxyUsers.Domain.Dto;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetDetailsByUserNameResponse : IResponse
    {
        #region [prop]
        public UserDetailedDto Details { get; set; }

        #endregion [prop]

        #region [ctor]

        public GetDetailsByUserNameResponse(UserDetailedDto userDetails)
        {
            Details = userDetails;
        }

        #endregion [ctor]
    }
}