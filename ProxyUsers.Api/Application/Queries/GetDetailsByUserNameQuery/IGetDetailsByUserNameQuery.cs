﻿using ProxyUsers.Api.Base.CQRS;

namespace ProxyUsers.Api.Application.Queries
{
    public interface IGetDetailsByUserNameQuery : IAsyncQueryHandler<GetDetailsByUserNameRequest, GetDetailsByUserNameResponse>
    {
    }
}
