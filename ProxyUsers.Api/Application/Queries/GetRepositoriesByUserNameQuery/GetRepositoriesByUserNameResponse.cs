﻿using ProxyUsers.Api.Base.CQ;
using ProxyUsers.Domain.Dto;
using System.Collections.Generic;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetRepositoriesByUserNameResponse : IResponse
    {
        #region [prop]
        public IEnumerable<RepositoryDto> Repositories { get; set; }

        #endregion [prop]

        #region [ctor]

        public GetRepositoriesByUserNameResponse(IEnumerable<RepositoryDto> repositories)
        {
            Repositories = repositories;
        }

        #endregion [ctor]
    }
}