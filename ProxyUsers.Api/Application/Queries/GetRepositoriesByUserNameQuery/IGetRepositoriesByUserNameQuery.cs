﻿using ProxyUsers.Api.Base.CQRS;

namespace ProxyUsers.Api.Application.Queries
{
    public interface IGetRepositoriesByUserNameQuery : IAsyncQueryHandler<GetRepositoriesByUserNameRequest, GetRepositoriesByUserNameResponse>
    {
    }
}
