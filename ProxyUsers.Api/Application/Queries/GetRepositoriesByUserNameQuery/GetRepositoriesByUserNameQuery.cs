﻿using ProxyUsers.Api.Base.CQRS;
using ProxyUsers.Domain.Contracts;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Core;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Infra.Data.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetRepositoriesByUserNameQuery : IGetRepositoriesByUserNameQuery
    {
        #region [prop]

        private readonly IUserRepository _userRepository;
        #endregion [prop]

        #region [ctor]

        public GetRepositoriesByUserNameQuery(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #endregion [ctor]


        public async Task<GetRepositoriesByUserNameResponse> ExecuteGetAsync(GetRepositoriesByUserNameRequest request, CancellationToken cancellationToken)
        {
            var response = await _userRepository.GetRepositoriesByUserNameAsync(request.UserName, cancellationToken);
            
            if (response == null || !response.Any() || response == default(IEnumerable<RepositoryDto>))
                return null;

            return new GetRepositoriesByUserNameResponse(response);
        }
    }
}