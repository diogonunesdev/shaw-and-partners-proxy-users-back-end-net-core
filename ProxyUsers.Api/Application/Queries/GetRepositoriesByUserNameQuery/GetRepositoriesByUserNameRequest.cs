﻿using ProxyUsers.Api.Base.CQ;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetRepositoriesByUserNameRequest : IRequest
    {
        #region [prop]
       public string UserName { get; set; }

        #endregion [prop]

        #region [ctor]

        public GetRepositoriesByUserNameRequest(string userName)
        {
            UserName = userName;
        }

        #endregion [ctor]
    }
}