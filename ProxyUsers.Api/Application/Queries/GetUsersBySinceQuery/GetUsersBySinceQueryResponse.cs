﻿using ProxyUsers.Api.Base.CQ;
using ProxyUsers.Domain.Dto;
using System.Collections.Generic;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetUsersBySinceResponse : IPaginatedResponse
    {
        #region [prop]
        public IEnumerable<UserDto> Users { get; private set; }

        public string next_page_url { get; set; }

        #endregion [prop]

        #region [ctor]

        public GetUsersBySinceResponse(IEnumerable<UserDto> users)
        {
            Users = users;
        }

        #endregion [ctor]
    }
}