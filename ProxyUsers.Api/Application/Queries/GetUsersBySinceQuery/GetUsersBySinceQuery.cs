﻿using ProxyUsers.Domain.Contracts;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Core;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Infra.Data.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Api.Application.Queries
{
    public sealed class GetUsersBySinceQuery : IGetUsersBySinceQuery
    {
        #region [prop]

        private readonly IPaginatedUserRepository _userRepository;
        private readonly IPaginatedRequest _paginatedRequest;
        #endregion [prop]

        #region [ctor]

        public GetUsersBySinceQuery(IPaginatedUserRepository userRepository, IPaginatedRequest paginatedRequest)
        {
            _userRepository = userRepository;
            _paginatedRequest = paginatedRequest;
        }

        #endregion [ctor]


        public async Task<GetUsersBySinceResponse> ExecuteGetAsync( CancellationToken cancellationToken)
        {
            var response = await _userRepository.GetUsersBySinceAsync(cancellationToken);

            if (response == null || !response.Any() || response == default(IEnumerable<UserDto>))
                return null;

            _paginatedRequest.SetLastSince(response.Max(w => w.id));
            return new GetUsersBySinceResponse(response);
        }
    }
}