﻿using ProxyUsers.Api.Base.CQRS;

namespace ProxyUsers.Api.Application.Queries
{
    public interface IGetUsersBySinceQuery : IAsyncQueryHandler<GetUsersBySinceResponse>
    {
    }
}
