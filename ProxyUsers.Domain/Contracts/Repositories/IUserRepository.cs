﻿using ProxyUsers.Domain.Dto;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Domain.Contracts.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<RepositoryDto>> GetRepositoriesByUserNameAsync(string userName, CancellationToken cancellationToken);

        Task<UserDetailedDto> GetUserByNameAsync(string userName, CancellationToken cancellationToken);
    }
}