﻿using ProxyUsers.Domain.Dto;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Domain.Contracts.Repositories
{
    public interface IPaginatedUserRepository
    {

        Task<IEnumerable<UserDto>> GetUsersBySinceAsync(CancellationToken cancellationToken);
    }
}