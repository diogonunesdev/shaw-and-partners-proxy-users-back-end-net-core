﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyUsers.Domain.Contracts
{
    public interface IDomainNotification
    {
        bool HasNotification { get; }

        IEnumerable<string> GetNotifications { get; }
        void AddRange(IEnumerable<string> notifications);
        void Add(string notification);
        void SetUniqueNotification(string notification);
        void Clear();
    }
}
