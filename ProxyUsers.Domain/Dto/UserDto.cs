﻿namespace ProxyUsers.Domain.Dto
{
    /// <summary>
    /// Anemic class
    /// </summary>
    public class UserDto
    {
        public string login { get; set; }
        public long id { get; set; }
    }
}
