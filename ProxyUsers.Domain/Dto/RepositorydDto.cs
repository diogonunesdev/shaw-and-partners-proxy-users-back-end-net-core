﻿/// <summary>
/// Anemic
/// </summary>
namespace ProxyUsers.Domain.Dto
{
    public sealed class RepositoryDto
    {
        public long id { get; set; }
        public string name { get; set; }
        public string html_url { get; set; }
    }
}