﻿using System;

namespace ProxyUsers.Domain.Dto
{
    /// <summary>
    /// Anemic class
    /// </summary>
    public sealed class UserDetailedDto : UserDto
    {
        public string html_url { get; set; }
        public DateTime created_at { get; set; }
    }
}
