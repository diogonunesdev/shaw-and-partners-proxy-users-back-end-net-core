## About

- This project was made from scratch, its architecture was based on the onion model
- I use Visual Studio 2019 With Dot Net Core 5.0

# Architectural Patterns
  - Clean Architecture (Onion)
  - MVC

---

# Pagination
  - Similiar of Offset and Limit pattern
  - Pagination Filter are configured for automatized this (request and response)
  - Page size can be between 10 and 50

---

# Design Patterns
  - IOC
  - DI
  - CQRS

---

# Tests
  - XUnit for Unit Tests
  - Specflow for Acceptance Tests
  - FluentAsertions for assertions
  - MOQ for Mocks
  - To run local tests go to Visual Studio 2019 > Test > Test Explores - Build the project and wait the Test Explorer show the tests
  - Have 5 Specflow + Gherkin Acceptance Test Writed
  - Have 19 Unit Tests Writed (i not write more because the time)

---

# Tunning
  - Response Caching are Enabled for two minutes in all routes
  - Response Compression with Brotli Optimization are Enabled
  - HttpClient are configured for each repository, with singleton scoped 
  - The business class is injected by the usage scoped lifetime, it is better than the transient lifetime because 
    the container does not need to create and destroy objects with each dependency injection, alleviating the 
    garbage collector and helping with memory management (transient lifecycle are not wrong, but, you need choose the right moment for the use)


---

# Security
  - For test reasons Cors policy are totally disabled
  - Note: I Know JWT Auth Pattern, but, for this sample i used fixed login in front-end

---

# Documentation
  - Swagger

---

# Configs
  - All configs in appsettings.json file (WebApi and Tests have the file)
  
  ---

# Notes

 - The Overengineering in architecture was purposeful, I wanted to show knowledge about some design patterns
  
# Deployed At
  
  - https://www.egal.com.br/swagger

# Thanks

 - \o/