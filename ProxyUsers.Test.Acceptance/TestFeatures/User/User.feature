﻿Feature: User
	Access User Api for Retrieve Users

@UsersApi
Scenario: Get All Users in First Page
	Given I have this pagination information
		| Since | PageSize |
		| 1     | 20       |
	When i request the first 20 users
	Then i get the 20 users requested

Scenario: Get All Users And Compare If Pagination Trade the Returned Values
	Given I have this pagination information
		| Since | PageSize |
		| 100   | 20       |
	And I have this pagination information for second request
		| Since | PageSize |
		| 1     | 20       |
	When i request the second 20 users
	Then the 21 user is differene off 1 user

Scenario: Get User By Login
	Given I have this pagination information
		| Since | PageSize |
		| 1     | 20       |
	When i request the first 20 users
	And  i choose one username and send the request to get user by username
	Then the user i was send have been returned with the same id

Scenario: Get Repositories By Login
	Given I have this pagination information
		| Since | PageSize |
		| 1     | 20       |
	When i request the first 20 users
	And  i choose one username and send the request to get user repositories by username
	Then the repositories of the user are returned

	
Scenario: Get Details By Login
	Given I have this pagination information
		| Since | PageSize |
		| 1     | 20       |
	When i request the first 20 users
	And  i choose one username and send the request to get user details by username
	Then the details of the user are returned