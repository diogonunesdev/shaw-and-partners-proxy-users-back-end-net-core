﻿using FluentAssertions;
using ProxyUsers.Api.Application.Queries;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Test.Acceptance.Core;
using ProxyUsers.Test.Acceptance.Core.Dto;
using ProxyUsers.Test.Acceptance.Core.TestHosts;
using System.Linq;
using System.Net.Http;
using TechTalk.SpecFlow;

namespace ProxyUsers.Test.Acceptance.Users.Steps
{
    [Binding]
    public sealed class UserSteps
    {
        private GetUsersBySinceRequestDto _paginationDto;
        private GetUsersBySinceRequestDto _pageTwopaginationDto;
        private HttpResponseMessage _apiCallResult;
        private HttpResponseMessage _secondApiCallResult;
        private UserDto _returnedUser;

        #region [Get All Users in First Page]

        [Given("I have this pagination information")]
        public void I_have_this_pagination_information(Table table)
        {
            _paginationDto = table.ToObject<GetUsersBySinceRequestDto>();
        }

        [When("i request the first 20 users")]
        public void i_request_the_first_20_users()
        {
            var message = ProxyUserApiTestServer.Client.Get("User", _paginationDto);
            _apiCallResult = ProxyUserApiTestServer.Client.SendAsync(message).Result;
        }

        [Then("i get the 20 users requested")]
        public void i_get_the_20_users_requested()
        {
            _apiCallResult.EnsureSuccessStatusCode();
            var response = _apiCallResult.GetResult<GetUsersBySinceResponse>().Result;
            response.Should().NotBeNull();
            response.Users.Should().NotBeNull();
            response.Users.Count().Should().Be(20);
            response.next_page_url.Should().NotBeEmpty();

            long getMaxSinceOfResponse = response.Users.Max(s => s.id);
            response.next_page_url.ToLower().Should().Contain($"since={getMaxSinceOfResponse}");
        }

        #endregion [Get All Users in First Page]

        #region [ Get All Users And Compare If Pagination Trade the Returned Values]

        [Given("I have this pagination information for second request")]
        public void I_have_this_pagination_information_for_second_request(Table table)
        {
            _pageTwopaginationDto = table.ToObject<GetUsersBySinceRequestDto>();
        }

        [When("i request the second 20 users")]
        public void i_request_the_second_20_users()
        {
            var messageFirstPagination = ProxyUserApiTestServer.Client.Get("User", _paginationDto);
            _apiCallResult = ProxyUserApiTestServer.Client.SendAsync(messageFirstPagination).Result;

            var messageSecondPagination = ProxyUserApiTestServer.Client.Get("User", _pageTwopaginationDto);
            _secondApiCallResult = ProxyUserApiTestServer.Client.SendAsync(messageSecondPagination).Result;
        }

        [Then("the 21 user is differene off 1 user")]
        public void the_21_user_is_differene_off_1_user()
        {
            _apiCallResult.EnsureSuccessStatusCode();
            var responseFirstCall = _apiCallResult.GetResult<GetUsersBySinceResponse>().Result;
            responseFirstCall.Should().NotBeNull();
            responseFirstCall.Users.Should().NotBeNull();
            responseFirstCall.Users.Count().Should().Be(20);

            _secondApiCallResult.EnsureSuccessStatusCode();
            var responseSecondCall = _secondApiCallResult.GetResult<GetUsersBySinceResponse>().Result;
            responseSecondCall.Should().NotBeNull();
            responseSecondCall.Users.Should().NotBeNull();
            responseSecondCall.Users.Count().Should().Be(20);

            long firstId = responseFirstCall.Users.First().id;
            long secondId = responseSecondCall.Users.First().id;

            firstId.Should().NotBe(secondId);
        }

        #endregion [Get All Users And Compare If Pagination Trade the Returned Values]

        #region [Get User By Login]

        [When("i choose one username and send the request to get user by username")]
        public void i_choose_one_username_and_send_the_request_to_get_user_by_username()
        {
            _returnedUser = _apiCallResult.GetResult<GetUsersBySinceResponse>().Result.Users.First();

            var message = ProxyUserApiTestServer.Client.Get($"user/{_returnedUser.login}/details");
            _apiCallResult = ProxyUserApiTestServer.Client.SendAsync(message).Result;
        }

        [Then("the user i was send have been returned with the same id")]
        public void the_user_i_was_send_have_been_returned_with_the_same_id()
        {
            _apiCallResult.EnsureSuccessStatusCode();
            var response = _apiCallResult.GetResult<GetDetailsByUserNameResponse>().Result;
            response.Should().NotBeNull();
            response.Details.id.Should().Be(_returnedUser.id);
        }

        #endregion [Get User By Login]

        #region [Get Repositories By Login]

        [When("i choose one username and send the request to get user repositories by username")]
        public void i_choose_one_username_and_send_the_request_to_get_user_repositories_by_username()
        {
            _returnedUser = _apiCallResult.GetResult<GetUsersBySinceResponse>().Result.Users.First();

            var message = ProxyUserApiTestServer.Client.Get($"user/{_returnedUser.login}/repos");
            _apiCallResult = ProxyUserApiTestServer.Client.SendAsync(message).Result;
        }

        [Then("the repositories of the user are returned")]
        public void the_repositories_of_the_user_are_returned()
        {

            _apiCallResult.EnsureSuccessStatusCode();
            var response = _apiCallResult.GetResult<GetRepositoriesByUserNameResponse>().Result;
            response.Should().NotBeNull();
            response.Repositories.Should().NotBeNull();
            response.Repositories.Count().Should().BeGreaterThan(0);
        }

        #endregion [Get Repositories By Login]
        
        #region [Get Details By Login]

        [When("i choose one username and send the request to get user details by username")]
        public void i_choose_one_username_and_send_the_request_to_get_user_details_by_username()
        {
            _returnedUser = _apiCallResult.GetResult<GetUsersBySinceResponse>().Result.Users.First();

            var message = ProxyUserApiTestServer.Client.Get($"user/{_returnedUser.login}/details");
            _apiCallResult = ProxyUserApiTestServer.Client.SendAsync(message).Result;
        }

        [Then("the details of the user are returned")]
        public void the_details_of_the_user_are_returned()
        {

            _apiCallResult.EnsureSuccessStatusCode();
            var response = _apiCallResult.GetResult<GetDetailsByUserNameResponse>().Result;
            response.Should().NotBeNull();
            response.Details.Should().NotBeNull();
            response.Details.id.Should().BeGreaterThan(0);
            response.Details.id.Should().Be(_returnedUser.id);
        }

        #endregion [Get Details By Login]
    }
}