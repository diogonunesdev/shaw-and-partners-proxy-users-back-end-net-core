﻿using Microsoft.AspNetCore.Hosting;
using ProxyUsers.Api;
using System;
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;

namespace ProxyUsers.Test.Acceptance.Core.TestHosts
{
    public static class ProxyUserApiTestServer
    {
        #region [prop]

        private const string ENVIRONMENT_DEVELOP = "Development";

        #endregion [prop]

        #region [ctor]

        static ProxyUserApiTestServer()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
        }

        #endregion [ctor]

        public static TestServer Server
        {
            get
            {
                if (_server == null)
                    _server = CreateTestServer();

                return _server;
            }
        }

        private static TestServer _server;

        public static HttpClient Client
        {
            get
            {
                if (_client == null)
                    _client = CreateServer();

                return _client;
            }
        }

        private static HttpClient _client;
        private static TestServer CreateTestServer()
        {
            var server = new TestServer(new WebHostBuilder()
        .ConfigureAppConfiguration((hostingContext, config) =>
            {
                var env = hostingContext.HostingEnvironment;

                config.AddJsonFile("appsettings.json", optional: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

                config.AddEnvironmentVariables();
            })
       .UseEnvironment(ENVIRONMENT_DEVELOP)
       .UseStartup<Startup>());

            return server;
        }

        public static HttpClient CreateServer()
        {
            var server = CreateTestServer();
            return server.CreateClient();
        }
    }
}