﻿namespace ProxyUsers.Test.Acceptance.Core.Dto
{
    public class GetUsersBySinceRequestDto
    {
        public long Since { get; set; }
        public int PageSize { get; set; }
    }
}