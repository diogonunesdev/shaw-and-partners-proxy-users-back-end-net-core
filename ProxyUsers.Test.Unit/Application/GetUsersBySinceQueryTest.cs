﻿using FluentAssertions;
using Moq;
using ProxyUsers.Api.Application.Queries;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Infra.Data.Contracts;
using ProxyUsers.Infra.Data.Request;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ProxyUsers.Test.Unit.Application
{
    public class GetUsersBySinceQueryTest
    {
        private readonly Mock<IPaginatedUserRepository> _mockUserRepository = new Mock<IPaginatedUserRepository>();
        private readonly IPaginatedRequest _paginatedRequest = new PaginatedRequest(0,0);
        private const int MAX_LENGTH_FAKE_DATA = 19;

        [Trait("Application", "GetUsersBySinceQuery")]
        [Fact]
        public async Task GetUsersBySinceQueryGetUsersAndLastSinceWithSuccess()
        {
            //arrange
            IGetUsersBySinceQuery query = new GetUsersBySinceQuery(_mockUserRepository.Object, _paginatedRequest);

            _mockUserRepository
                .Setup(s => s.GetUsersBySinceAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(GetFakeData());

            //act
            var result = await query.ExecuteGetAsync(CancellationToken.None);

            //assert
            result.Should().NotBeNull();
            result.Users.Should().NotBeEmpty();
            _paginatedRequest.LastSince.Should().Be(MAX_LENGTH_FAKE_DATA);
            _mockUserRepository.Verify(v => v.GetUsersBySinceAsync(It.IsAny<CancellationToken>()), Times.Exactly(1));
        }

        private IEnumerable<UserDto> GetFakeData()
        {
            for (int i = 0; i <= MAX_LENGTH_FAKE_DATA; i++)
                yield return new UserDto
                {
                    id = i,
                    login = $"login_{i}"
                };
        }
    }
}
