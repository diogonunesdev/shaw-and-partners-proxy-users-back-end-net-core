﻿using Microsoft.Extensions.Logging;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Infra.Data.Contracts;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Infra.Data.Repositories
{
    public sealed class PaginatedUserRepository : BasePaginatedHttpRepository, IPaginatedUserRepository
    {
        #region [prop]

        private const string USER_ACTION = "users";

        #endregion [prop]

        #region [ctor]

        public PaginatedUserRepository(HttpClient httpClient,
            IPaginatedRequest paginatedRequest,
             ILogger logger) : base(httpClient, paginatedRequest, logger)
        {
        }

        #endregion [ctor]

        public async Task<IEnumerable<UserDto>> GetUsersBySinceAsync(CancellationToken cancellationToken)
        {
            var response = await GetAsync<IEnumerable<UserDto>>(USER_ACTION, cancellationToken);
            return response;
        }
    }
}