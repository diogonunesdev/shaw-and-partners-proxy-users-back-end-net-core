﻿using Microsoft.Extensions.Logging;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Dto;
using ProxyUsers.Infra.Data.Contracts;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ProxyUsers.Infra.Data.Repositories
{
    public sealed class UserRepository : BaseHttpRepository, IUserRepository
    {
        #region [prop]

        private const string USER_ACTION = "users";

        #endregion [prop]

        #region [ctor]

        public UserRepository(HttpClient httpClient,
            IPaginatedRequest paginatedRequest,
            ILogger logger) : base(httpClient, logger)
        {
        }

        #endregion [ctor]

        public async Task<UserDetailedDto> GetUserByNameAsync(string userName, CancellationToken cancellationToken)
        {
            var response = await GetAsync<UserDetailedDto>($"{USER_ACTION}/{userName}", cancellationToken);
            return response;
        }

        public async Task<IEnumerable<RepositoryDto>> GetRepositoriesByUserNameAsync(string userName, CancellationToken cancellationToken)
        {
            var response = await GetAsync<IEnumerable<RepositoryDto>>($"{USER_ACTION}/{userName}/repos", cancellationToken);
            return response;
        }

    }
}