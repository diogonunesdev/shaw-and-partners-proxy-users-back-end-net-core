﻿using Microsoft.Extensions.Logging;
using ProxyUsers.Infra.Data.Contracts;
using System.Net.Http;

namespace ProxyUsers.Infra.Data.Repositories
{
    public abstract class BasePaginatedHttpRepository : BaseHttpRepository
    {
        #region [prop]

        private readonly IPaginatedRequest _paginatedRequest;

        #endregion [prop]

        #region [ctor]

        protected BasePaginatedHttpRepository(HttpClient httpClient, IPaginatedRequest paginatedRequest, ILogger logger) : base(httpClient,logger)
        {
            _paginatedRequest = paginatedRequest;
        }

        #endregion [ctor]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeAction">Action</param>
        /// <param name="page">Current Page</param>
        /// <param name="pageSize">Itens per page</param>
        /// <returns></returns>
        protected override string GetRoute(string routeAction)
        {
            return routeAction.Contains("?")
                ? $"{base.GetRoute(routeAction)}&per_page={_paginatedRequest.PageSize}&since={_paginatedRequest.Since}"
                : $"{ base.GetRoute(routeAction)}?per_page={_paginatedRequest.PageSize}&since={_paginatedRequest.Since}";
        }
    }
}