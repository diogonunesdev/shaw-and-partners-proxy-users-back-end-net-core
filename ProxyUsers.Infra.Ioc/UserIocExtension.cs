﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProxyUsers.Domain.Contracts;
using ProxyUsers.Domain.Contracts.Repositories;
using ProxyUsers.Domain.Core;
using ProxyUsers.Infra.Data.Contracts;
using ProxyUsers.Infra.Data.Repositories;
using ProxyUsers.Infra.Data.Request;
using System;
using System.Net.Http;


namespace ProxyUsers.Infra.Ioc
{
    public static class UserIocExtension
    {
        public static void ConfigureUserIocServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IDomainNotification, DomainNotification>();

            services.AddScoped<IPaginatedRequest, PaginatedRequest>(c => new PaginatedRequest(0,0));

            services.AddHttpClient<IPaginatedUserRepository, PaginatedUserRepository>()
                      .SetHandlerLifetime(TimeSpan.FromMinutes(5))
                     .ConfigureHttpClient(client => ConfigureHttpClienteForGitHubAccess(client, configuration));

            services.AddHttpClient<IUserRepository, UserRepository>()
                   .SetHandlerLifetime(TimeSpan.FromMinutes(5))
                  .ConfigureHttpClient(client => ConfigureHttpClienteForGitHubAccess(client, configuration));

        }

        #region [Configure HttpClient]

        private static void ConfigureHttpClienteForGitHubAccess(HttpClient client, IConfiguration configuration)
        {
            client.BaseAddress = new Uri(configuration.GetValue<string>("ExternalApi:GitHub:BaseUri"));
            var token = configuration.GetValue<string>("ExternalApi:GitHub:AccessToken");

            client.DefaultRequestHeaders.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue("AppName", "1.0"));
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", token);
        }

        #endregion [Configure HttpClient]
    }
}
